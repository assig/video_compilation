# coding: utf-8
import urllib
import urllib2
from bs4 import BeautifulSoup
from pytube import YouTube


# textToSearch = 'road rash 2'
# query = urllib.quote(textToSearch)
# url = "https://www.youtube.com/results?sp=EgIYAQ%253D%253D&q=" + query
# response = urllib2.urlopen(url)
# html = response.read()
# soup = BeautifulSoup(html, 'html.parser')
# for vid in soup.findAll(attrs={'class':'yt-uix-tile-link'}):
#     print 'https://www.youtube.com' + vid['href']
#
# video_first = soup.findAll(attrs={'class':'yt-uix-tile-link'})[0]
# print "video first " + video_first['href']
#
# video_second = soup.findAll(attrs={'class':'yt-uix-tile-link'})[1]
# print "video first " + video_second['href']
# yt = YouTube('https://www.youtube.com'+video_first['href'])
# print yt.filename
# print(yt.filter('mp4')[-1])
# print yt.videos
# video = yt.get('mp4', '360p')
# video.download('/tmp/')


import htmlPy
import os
from video_compilation_package.views import Views
from video_compilation_package.data_base import DataBase


app = htmlPy.AppGUI(title=u"Видео Компилятор", maximized=True)
app.template_path = os.path.abspath("templates")
app.static_path = os.path.abspath("static")

db = DataBase()
videos = db.get_videos()
projects = db.get_projects()

app.template = ("index.html", {"videos": videos, 'projects': projects})
app.bind(Views())
if __name__ == "__main__":
    app.start()

