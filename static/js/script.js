/**
 * Created by assig on 6/19/16.
 */
set_controls(1, true);


$('.add_column').click(function(){
  var table_body = $('.table_body');
  var id = Views.get_last_id();
  var index = table_body.find('tr').length+1;
  var new_tr = '<tr>'+
      '<td class="id" data-id="'+ id +'">'+ index +'</td>'+
      '<td><input class="query form-control" type="text"></td>'+
      '<td><input class="number" id="number_'+ index +'" value="1"></td>'+
      '<td>'+
        '<select class="duration" name="duration" id="duration_'+ index +'">'+
          '<option value="short">Меньше 4мин.</option>'+
          '<option value="medium">От 4мин. до 20мин.</option>'+
          '<option value="long">Больше 20мин.</option>'+
        '</select>'+
      '</td>' +
    '<td><div class="add_video"></div></td>'+
      '<td class="slider_td" ></td>'+
      '<td><input class="form-control" type="text"></td>'+
    '</tr>';
  table_body.append(new_tr);
  set_controls(index, true);
});


$('.table_body').on('change', '.query', function(){
    var query = $(this).val();
    var parent = $(this).parent().parent();
    var number = parent.find('.number').val();
    var duration = parent.find('.duration').val();
    var id = parent.find('.id').data('id');
    send_to_search(id, query, number, duration, parent);
  });


function set_controls(index, search) {
  $('#duration_'+index).selectmenu({
    change: function( event, ui ) {
      var duration = $(this).val();
      var parent = $(this).parent().parent();
      var query = parent.find('.query').val();
      var number = parent.find('.number').val();
      var id = parent.find('.id').data('id');
      send_to_search(id, query, number, duration, parent);
    }
  });

  $('#number_'+index).spinner({
    min: 1,
    max: 50,
    change: function( event, ui ) {
      var number = $(this).val();
      var parent = $(this).parent().parent().parent();
      var query = parent.find('.query').val();
      var duration = parent.find('.duration').val();
      var id = parent.find('.id').data('id');
      send_to_search(id, query, number, duration, parent);
    }
  });
}


function send_to_search(id, query, number, duration, parent){
  var json_data = JSON.parse(Views.search(id, query, parseInt(number), duration));
  var video_id = json_data.video_id;
  var max_second = json_data.max_second;
  var video_html = '<div class="video" data-type="youtube" data-video-id="'+video_id+'"></div>';
  parent.find('.add_video').html(video_html);
  plyr.setup();
  set_slider(max_second, 0, max_second, parent)

}


function set_slider(max_second, min, max, parent) {
  parent.find('.slider_td').html('<div class="slider"></div>');
  var slider = parent.find('.slider').rangeSlider({
     bounds: {min: 0, max: max_second},
     defaultValues:{min: min, max: max}
  });
  slider.bind("valuesChanged", function(e, data){
    $('.log').html(data.values.max);
  });
}

function set_video(video_id, parent){
   var video_html = '<div class="video" data-type="youtube" data-video-id="'+video_id+'"></div>';
   parent.find('.add_video').html(video_html);
   plyr.setup();
}


$('.create_project').click(function(){
  Views.new_project($('new_project_name').val());
});
