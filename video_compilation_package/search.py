from youtubeapi import YoutubeAPI
import json
import urllib
import isodate

API_KEY = 'AIzaSyAwDbeX_PNi2gVs3XwQYA-J-GGL1eiwj3M'


def search_query(query, number, duration):
    params = {
        'q': query.encode('utf-8'),
        'type': 'video',
        'part': 'id',
        'maxResults': 50,
        'videoDuration': duration
    }
    youtube = YoutubeAPI({'key': API_KEY})
    results = youtube.search_advanced(params)
    res = results[number-1]
    id_video = res['id']['videoId']
    return id_video


def search_max_second(video_id):
    searchUrl = "https://www.googleapis.com/youtube/v3/videos?id="+video_id+"&key="+API_KEY+"&part=contentDetails"
    response = urllib.urlopen(searchUrl).read()
    data = json.loads(response)
    all_data = data['items']
    contentDetails = all_data[0]['contentDetails']
    duration_8601 = contentDetails['duration']
    duration = isodate.parse_duration(duration_8601)
    return int(duration.total_seconds())
