import htmlPy
import json
from .search import search_query, search_max_second
from .data_base import DataBase


class Views(htmlPy.Object):
    @htmlPy.Slot(int, str, int, str, result=str)
    def search(self, id_, query, number, duration):
        video_id = search_query(query, number, duration)
        max_second = search_max_second(video_id)
        db = DataBase()
        db.add_or_replace(id_, query, number, duration, video_id, max_second)
        data = {'video_id': video_id, 'max_second': max_second}
        return json.dumps(data)

    @htmlPy.Slot(int, int, result=str)
    def save_range(self, min, max):
        return 'True'

    @htmlPy.Slot(result=int)
    def get_last_id(self):
        db = DataBase()
        return db.get_last_id() + 1

    # project methods:
    @htmlPy.Slot(str, result=str)
    def new_project(self, json_data):
        form_data = json.loads(json_data)
        project_name = form_data['project_name']
        db = DataBase()
        db.add_project(project_name)
        projects = db.get_projects()
        # app_.template("index.html", {"videos": [], 'projects': projects})

