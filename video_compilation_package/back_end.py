import htmlPy


class BackEnd(htmlPy.Object):

    @htmlPy.Slot()
    def say_hello_world(self):
        from main import app as APP
        APP.stop()
        app = htmlPy.AppGUI(
            title=u"Sample application")
        app.maximized = True
        app.template_path = "."
        app.bind(BackEnd())

        app.template = ("index.html", {})
        app.html = u"Hello, world"
        app.start()