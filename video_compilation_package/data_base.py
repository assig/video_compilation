import sqlite3


def _dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d


class DataBase(object):

    def __init__(self):
        self.conn = sqlite3.connect('sqlite')
        self.conn.row_factory = _dict_factory
        self.cursor = self.conn.cursor()

    def __del__(self):
        self.cursor.close()
        self.conn.close()

    # add methods:
    def add_or_replace(self, id_, query, number, duration, video_id, slide_max):
        active_project_id = self.get_active_project_id()
        self.cursor.execute('INSERT OR REPLACE INTO videos'
                            '(id, id_project, query, number, duration, video_id, slide_min,'
                            ' slide_max, slide_max_second) '
                            'values(?,?,?,?,?,?,?,?,?)',
                            (id_, active_project_id, query, number, duration, video_id, 0, slide_max, slide_max))
        self.conn.commit()
        return "SUCCESS INSERT OR UPDATE"

    def add_project(self, project_name):
        self.cursor.execute('UPDATE projects SET active=?', (0,))
        self.cursor.execute('INSERT INTO projects(name, active) VALUES (?,?)',
                            (project_name, 1))
        self.conn.commit()
        return "SUCCESS INSERT NEW PROJECT"

    # update methods:
    def update_slide(self, id_, slide_min, slide_max):
        self.cursor.execute('UPDATE videos SET slide_min=?, slide_max=? WHERE id=?',
                            (slide_min, slide_max, id_))
        self.conn.commit()
        return "SUCCESS UPDATE"

    def update_video_text(self, id_, video_text):
        self.cursor.execute('UPDATE videos SET video_text=? WHERE id=?',
                            (video_text, id_))
        self.conn.commit()
        return "SUCCESS UPDATE"

    def set_active_project_id(self, active_project_id):
        self.cursor.execute('UPDATE projects SET active=?', (0,))
        self.cursor.execute('UPDATE projects SET active=? WHERE id=?', (1, active_project_id))
        return "SUCCESS SET ACTIVE PROJECT"

    # get methods:
    def get_videos(self):
        active_project_id = self.get_active_project_id()
        self.cursor.execute('SELECT * FROM videos WHERE id_project=?', (active_project_id,))
        return self.cursor.fetchall()

    def get_projects(self):
        self.cursor.execute('SELECT * FROM projects')
        return self.cursor.fetchall()

    def get_active_project_id(self):
        self.cursor.execute('SELECT * FROM projects WHERE active=1')
        if self.cursor.fetchone():
            return self.cursor.fetchone()['id']

    def get_last_id(self):
        self.cursor.execute('SELECT id FROM videos ORDER BY id DESC LIMIT 1')
        return self.cursor.fetchone()['id']

    # delete methods:
    def delete_video(self, video_id):
        self.cursor.execute('DELETE FROM videos WHERE id=?', (video_id,))

    # db create method:
    def create_bases(self):
        self.cursor.execute('''CREATE TABLE projects
                            (id INTEGER PRIMARY KEY,
                             name TEXT,
                             active INTEGER);''')
        self.cursor.execute('''CREATE TABLE videos
                            (id INTEGER PRIMARY KEY,
                            id_project REFERENCES projects(id),
                             query TEXT,
                             number INTEGER,
                             duration TEXT,
                             video_id TEXT,
                             slide_min INTEGER,
                             slide_max INTEGER,
                             slide_max_second INTEGER,
                             video_text TEXT);''')
        return "Data Bases Created"
